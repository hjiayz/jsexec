// Copyright 2022 hjiayz
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.



fn main(){
    use std::env;

    let wasi_sdk_path = "./wasi-sdk";

    std::env::set_var("CC", format!("{}/bin/clang", &wasi_sdk_path));
    env::set_var("AR", format!("{}/bin/ar", &wasi_sdk_path));
    let sysroot = format!("--sysroot={}/share/wasi-sysroot", &wasi_sdk_path);
    env::set_var("CFLAGS", &sysroot);
    
    let mut build = cc::Build::new();




    build.define("_GNU_SOURCE", None);
    build.define("__wasi__",None);
    build.define("CONFIG_VERSION", "\"2021-03-27\"");
    build.define("CONFIG_BIGNUM", None);
    build.opt_level(2);
    build.cargo_metadata(true);
    build.flag_if_supported("-g");
    build.flag_if_supported("-Wall");
    build.flag_if_supported("-Wextra");
    build.flag_if_supported("-Wno-sign-compare");
    build.flag_if_supported("-Wno-unused-parameter");
    build.flag_if_supported("-Wuninitialized");
    build.flag_if_supported("-Wwrite-strings");
    build.flag_if_supported("-Wchar-subscripts");
    build.flag_if_supported("-funsigned-char");

    build.flag_if_supported("-Wno-array-bounds");
    build.flag_if_supported("-Wno-format-truncation");

    build.flag_if_supported("-Wno-implicit-fallthrough");
    build.flag_if_supported("-Wno-missing-field-initializers");
    build.flag_if_supported("-Wundef");
    build.flag_if_supported("-Wno-implicit-const-int-float-conversion");
    build.flag_if_supported("-D_WASI_EMULATED_SIGNAL");
    build.flag_if_supported("-lwasi-emulated-signal");
    

    let files = [
    "libregexp.c",
    "libunicode.c",
    "cutils.c",
    "quickjs.c",
    "libbf.c",
    ];
    for file in files {
        build.file(format!("c/quickjs/{file}"));
    };
    build.file("c/runqjs.c").compile("qjs.a");

    println!("cargo:rerun-if-changed=c/runqjs.c");
    println!("cargo:rerun-if-changed=build.rs");
}