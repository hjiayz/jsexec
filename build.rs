use std::{path::PathBuf, str::FromStr};

use wasmer::*;
use wasmer_compiler_cranelift::Cranelift;

fn main() {
    for var in std::env::vars() {
        dbg!(var);
    }
    let target_str = std::env::var("TARGET").expect("unknown TARGET");
    let features_strs =
        std::env::var("CARGO_CFG_TARGET_FEATURE").expect("unknown CARGO_CFG_TARGET_FEATURE");
    let compiler_config = Cranelift::default();
    let triple = Triple::from_str(&target_str).expect("unknown triple");
    let mut cpu_features = CpuFeature::set();
    for feature in features_strs.split(",") {
        if let Ok(feature) = CpuFeature::from_str(feature) {
            cpu_features.insert(feature);
        }
    }
    let target = Target::new(triple, cpu_features);

    let engine = EngineBuilder::new(compiler_config)
        .set_target(Some(target))
        .engine();

    let store = Store::new(engine);
    let module_wasm = include_bytes!("src/qjs.wasm");
    let module = Module::new(&store, &module_wasm).expect("compile failed");
    let cmd = std::env::var("OUT_DIR").expect("unknown OUT_DIR");
    let mut dir = PathBuf::from_str(&cmd).expect("bad OUT_DIR");
    dir.push("qjs.lib");
    module.serialize_to_file(dir).expect("failed to save file");
    println!("cargo:rerun-if-changed=build.rs");
    println!("cargo:rerun-if-changed=src/qjs.wasm");
    println!("cargo:rerun-if-changed=src/lib.rs");
}
